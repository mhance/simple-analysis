#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <unordered_set>
#include <algorithm>

//---------------------------------------------------------
//  
//  SUSY EWK Compressed 'Higgsino' Analysis
//  ANA-SUSY-2018-16
//
//---------------------------------------------------------

DefineAnalysis(EwkCompressed2018)

void EwkCompressed2018::Init()
{

  //Electroweakino exclusive SRs
  //SR-E-lT, exclusive mll bins (0.5, 1, 1.5, 2, 3, 4, and 5 GeV)
  addRegions( {"SR_E_lT_eMLLa","SR_E_lT_eMLLb","SR_E_lT_eMLLc","SR_E_lT_eMLLd","SR_E_lT_eMLLe","SR_E_lT_eMLLf"} );
  //SR-E-high, exclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40, and 60 GeV)
  addRegions( {"SR_E_high_eMLLa","SR_E_high_eMLLb","SR_E_high_eMLLc","SR_E_high_eMLLd","SR_E_high_eMLLe","SR_E_high_eMLLf","SR_E_high_eMLLg","SR_E_high_eMLLh"} );
  //SR-E-med, exclusive mll bins (1, 2, 3, 5, 10, 20 and 30 GeV)
  addRegions( {"SR_E_med_eMLLa","SR_E_med_eMLLb","SR_E_med_eMLLc","SR_E_med_eMLLd","SR_E_med_eMLLe","SR_E_med_eMLLf"} );
  //SR-E-low, exclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40, and 60 GeV)
  addRegions( {"SR_E_low_eMLLa","SR_E_low_eMLLb","SR_E_low_eMLLc","SR_E_low_eMLLd","SR_E_low_eMLLe","SR_E_low_eMLLf","SR_E_low_eMLLg","SR_E_low_eMLLh"} );

  //Electroweakino inclusive SRs
  //SR-E-lT, inclusive mll bins (0.5, 1, 2, 3, 5, 10, 20, 30, 40, and 60 GeV)
  addRegions( {"SR_E_lT_iMLLa","SR_E_lT_iMLLb","SR_E_lT_iMLLc","SR_E_lT_iMLLd","SR_E_lT_iMLLe","SR_E_lT_iMLLf","SR_E_lT_iMLLg","SR_E_lT_iMLLh","SR_E_lT_iMLLi"} );
  //SR-E-high, inclusive mll bins (0.5, 1, 2, 3, 5, 10, 20, 30, 40, and 60 GeV)
  addRegions( {"SR_E_high_iMLLa","SR_E_high_iMLLb","SR_E_high_iMLLc","SR_E_high_iMLLd","SR_E_high_iMLLe","SR_E_high_iMLLf","SR_E_high_iMLLg","SR_E_high_iMLLh","SR_E_high_iMLLi"} );
  //SR-E-med, inclusive mll bins (0.5, 1, 2, 3, 5, 10, 20, 30, 40, and 60 GeV)
  addRegions( {"SR_E_med_iMLLa","SR_E_med_iMLLb","SR_E_med_iMLLc","SR_E_med_iMLLd","SR_E_med_iMLLe","SR_E_med_iMLLf","SR_E_med_iMLLg","SR_E_med_iMLLh","SR_E_med_iMLLi"} );
  //SR-E-low, inclusive mll bins (0.5, 1, 2, 3, 5, 10, 20, 30, 40, and 60 GeV)
  addRegions( {"SR_E_low_iMLLa","SR_E_low_iMLLb","SR_E_low_iMLLc","SR_E_low_iMLLd","SR_E_low_iMLLe","SR_E_low_iMLLf","SR_E_low_iMLLg","SR_E_low_iMLLh","SR_E_low_iMLLi"} );
  //SR-E OR, inclusive mll bins (0.5, 1, 2, 3, 5, 10, 20, 30, 40, and 60 GeV)
  addRegions( {"SR_E_iMLLa","SR_E_iMLLb","SR_E_iMLLc","SR_E_iMLLd","SR_E_iMLLe","SR_E_iMLLf","SR_E_iMLLg","SR_E_iMLLh","SR_E_iMLLi"} );

  //Slepton exclusive SRs
  //SR-S-high, exclusive mT2 bins (100, 100.5, 101, 102, 105, 110, 120, 130, and 140 GeV)
  addRegions( {"SR_S_high_eMT2a","SR_S_high_eMT2b","SR_S_high_eMT2c","SR_S_high_eMT2d","SR_S_high_eMT2e","SR_S_high_eMT2f","SR_S_high_eMT2g","SR_S_high_eMT2h"} );
  //SR-S-low, exclusive mT2 bins (100, 100.5, 101, 102, 105, 110, 120, 130, and 140 GeV)
  addRegions( {"SR_S_low_eMT2a","SR_S_low_eMT2b","SR_S_low_eMT2c","SR_S_low_eMT2d","SR_S_low_eMT2e","SR_S_low_eMT2f","SR_S_low_eMT2g","SR_S_low_eMT2h"} );

  //Slepton inclusive SRs
  //SR-S-high, inclusive mT2 bins (100, 100.5, 101, 102, 105, 110, 120, 130, and 140 GeV)
  addRegions( {"SR_S_high_iMT2a","SR_S_high_iMT2b","SR_S_high_iMT2c","SR_S_high_iMT2d","SR_S_high_iMT2e","SR_S_high_iMT2f","SR_S_high_iMT2g","SR_S_high_iMT2h"} );
  //SR-S-low, inclusive mT2 bins (100, 100.5, 101, 102, 105, 110, 120, 130, and 140 GeV)
  addRegions( {"SR_S_low_iMT2a","SR_S_low_iMT2b","SR_S_low_iMT2c","SR_S_low_iMT2d","SR_S_low_iMT2e","SR_S_low_iMT2f","SR_S_low_iMT2g","SR_S_low_iMT2h"} );
  //SR-S OR, inclusive mT2 bins (100, 100.5, 101, 102, 105, 110, 120, 130, and 140 GeV)
  addRegions( {"SR_S_iMT2a","SR_S_iMT2b","SR_S_iMT2c","SR_S_iMT2d","SR_S_iMT2e","SR_S_iMT2f","SR_S_iMT2g","SR_S_iMT2h"} );

  //VBF exclusive SRs
  //SR-VBF-high, exclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_high_eMLLa","SR_VBF_high_eMLLb","SR_VBF_high_eMLLc","SR_VBF_high_eMLLd","SR_VBF_high_eMLLe","SR_VBF_high_eMLLf","SR_VBF_high_eMLLg"} );
  //SR-VBF-low, exclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_low_eMLLa","SR_VBF_low_eMLLb","SR_VBF_low_eMLLc","SR_VBF_low_eMLLd","SR_VBF_low_eMLLe","SR_VBF_low_eMLLf","SR_VBF_low_eMLLg"} );

  //VBF inclusive SRs
  //SR-VBF-high, inclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_high_iMLLa","SR_VBF_high_iMLLb","SR_VBF_high_iMLLc","SR_VBF_high_iMLLd","SR_VBF_high_iMLLe","SR_VBF_high_iMLLf","SR_VBF_high_iMLLg"} );
  //SR-VBF-low, inclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_iMLLa","SR_VBF_iMLLb","SR_VBF_iMLLc","SR_VBF_iMLLd","SR_VBF_iMLLe","SR_VBF_iMLLf","SR_VBF_iMLLg"} );

  //addHistogram("Met",20,0,2000);

  //---------------------------------------------------------
  //
  //                   Intitialize RJR
  //
  //---------------------------------------------------------

  LabRecoFrame* LAB        = m_RF_helper.addLabFrame("LAB");
  DecayRecoFrame* CM       = m_RF_helper.addDecayFrame("CM");
  DecayRecoFrame* S        = m_RF_helper.addDecayFrame("S");
  VisibleRecoFrame* ISR    = m_RF_helper.addVisibleFrame("ISR");
  VisibleRecoFrame* V      = m_RF_helper.addVisibleFrame("V");
  VisibleRecoFrame* L      = m_RF_helper.addVisibleFrame("L");
  InvisibleRecoFrame* I    = m_RF_helper.addInvisibleFrame("I");

  InvisibleGroup* INV      = m_RF_helper.addInvisibleGroup("INV");

  // set the invisible system mass to zero
  InvisibleJigsaw* InvMass = m_RF_helper.addInvisibleJigsaw("InvMass", kSetMass);
  //kSetMass does InvisibleJigsaw = new SetMassInvJigsaw("InvMass","InvMass");

  CombinatoricGroup* VIS   = m_RF_helper.addCombinatoricGroup("VIS");

  // define the rule for partitioning objects between "ISR" and "V"
  MinMassesCombJigsaw* SplitVis = m_RF_helper.addCombinatoricJigsaw("SplitVis", kMinMasses);
  //kMinMasses does MinMassesCombJigsaw = new MinMassesCombJigsaw(SplitVis,SplitVis);

  LAB->SetChildFrame(*CM);
  CM->AddChildFrame(*ISR);
  CM->AddChildFrame(*S);
  S->AddChildFrame(*V);
  S->AddChildFrame(*I);
  S->AddChildFrame(*L);

  LAB->InitializeTree();

  INV->AddFrame(*I);

  VIS->AddFrame(*ISR);
  VIS->SetNElementsForFrame(*ISR,1,false);
  VIS->AddFrame(*V);
  VIS->SetNElementsForFrame(*V,0,false);

  INV->AddJigsaw(*InvMass);

  VIS->AddJigsaw(*SplitVis);
  // "0" group (ISR)
  SplitVis->AddFrame(*ISR, 0);
  // "1" group (V + I + L)
  SplitVis->AddFrame(*V,1);
  SplitVis->AddFrame(*I,1);
  SplitVis->AddFrame(*L,1);

  LAB->InitializeAnalysis();

}

//---------------------------------------------------------

//---------------------------------------------------------  

void EwkCompressed2018::ProcessEvent(AnalysisEvent *event)
{
  //---------------------------------------------------------
  //
  //                   Prepare objects
  //
  //---------------------------------------------------------

  auto baseElectrons  = event->getElectrons(4.5, 2.47, EVeryLooseLH);
  auto baseMuons      = event->getMuons(3, 2.5, MuMedium); //actually lowPt
  auto preJets        = event->getJets(20., 4.5); 
  auto metVec         = event->getMET();
  float met = metVec.Pt();

  // Reject events with bad jets
  if (countObjects(preJets, 20, 4.5, NOT(LooseBadJet))!=0) return;

  // hard lepton overlap removal and signal lepton/jet definitions
  auto baseJets        = overlapRemoval(preJets,       baseElectrons, 0.2, NOT(BTag85MV2c10));
       baseElectrons   = overlapRemoval(baseElectrons, baseJets,      0.4);
       baseElectrons   = overlapRemoval(baseElectrons, baseMuons,     0.01);
       baseJets        = overlapRemoval(baseJets,      baseMuons,     0.4, LessThan3Tracks);
       baseMuons       = overlapRemoval(baseMuons,     baseJets, 0.4);
  auto signalJets      = filterObjects( baseJets,      30,  2.8,  JVT50Jet);
  auto signalBJets     = filterObjects( baseJets,      20,  2.5,  BTag85MV2c10);
  auto signalElectrons = filterObjects( baseElectrons, 4.5, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradient);
  auto signalMuons     = filterObjects( baseMuons,     3,   2.5,  MuD0Sigma3|MuZ05mm|MuIsoFixedCutTightTrackOnly);

  AnalysisObjects baseLeptons   = baseElectrons+baseMuons;
  AnalysisObjects signalLeptons = signalElectrons+signalMuons;

  //get tracks within R < 0.01 of reconstructed electron or muon candidate
  auto preElTracks   = event->getElectrons(0.5, 2.5);
  auto preMuTracks   = event->getMuons(0.5, 2.5);

  // reconstructed electron or muon candidate must fail signal lepton criteria
  AnalysisObjects baseElTracks;
  for (const auto& preElTrack : preElTracks) {
    bool match = false;
    for (const auto& signalElectron : signalElectrons) {
      if (preElTrack == signalElectron) {
        match = true;
        break;
      }
    }
    if (!match) baseElTracks.push_back(preElTrack);
  }

  AnalysisObjects baseMuTracks;
  for (const auto& preMuTrack : preMuTracks) {
    bool match = false;
    for (const auto& signalMuon : signalMuons) {
      if (preMuTrack == signalMuon) {
        match = true;
        break;
      }
    }
    if (!match) baseMuTracks.push_back(preMuTrack);
  }

  //baseline tracks do not have signal leptons
  AnalysisObjects baseTracks   = baseElTracks+baseMuTracks;

  // Tracks are required to be separated from preselected jets by at least dR > 0.5
  baseElTracks = overlapRemoval(baseElTracks, baseJets, 0.5);
  baseMuTracks = overlapRemoval(baseMuTracks, baseJets, 0.5);

  // sum pT of preselected tracks within dR < 0.3 of signal tracks is required to be smaller than 0.5 GeV (basically means nothing within 0.3 of the track)
  auto signalElTracks = overlapRemoval(baseElTracks, baseTracks, 0.3);
  auto signalMuTracks = overlapRemoval(baseMuTracks, baseTracks, 0.3);
    
  // tracks must pass pT, z0 and d0 requirements
  signalElTracks = filterObjects(signalElTracks, 1, 2.5, ED0Sigma5|EZ05mm);
  signalMuTracks = filterObjects(signalMuTracks, 1, 2.5, MuD0Sigma3|MuZ05mm);

  AnalysisObjects signalTracks = signalElTracks+signalMuTracks;

  // Preselect at least 1 baseline lepton
  // Final selection requires exactly 1 or exactly 2 signal leptons
  if ( !(baseLeptons.size() >=1) ) return;
  // preselect at least one signal jet because this is an ISR analysis
  // also RJR doesn't work without this...
  if ( !(signalJets.size() >= 1) ) return;
  
  //---------------------------------------------------------
  //
  //                Calculate RJR variables
  //
  //---------------------------------------------------------

  // analyze event in RestFrames tree
  // Combinatoric setup for jets
  LabRecoFrame* LAB = m_RF_helper.getLabFrame("LAB");
  CombinatoricGroup* VIS = m_RF_helper.getCombinatoricGroup("VIS");
  InvisibleGroup* INV = m_RF_helper.getInvisibleGroup("INV");
  DecayRecoFrame* CM = m_RF_helper.getDecayFrame("CM");
  DecayRecoFrame* S = m_RF_helper.getDecayFrame("S");
  VisibleRecoFrame* ISR = m_RF_helper.getVisibleFrame("ISR");
  VisibleRecoFrame* V = m_RF_helper.getVisibleFrame("V");
  VisibleRecoFrame* L = m_RF_helper.getVisibleFrame("L");
  InvisibleRecoFrame* I = m_RF_helper.getInvisibleFrame("I");

  LAB->ClearEvent();

  std::vector<RFKey> jetID;
  for (const auto& jet : signalJets){
    jetID.push_back(VIS->AddLabFrameFourVector(jet.transFourVect()));
  } 

  TLorentzVector lepSys(0.,0.,0.,0.);
  for(const auto lep1 : signalLeptons){
    lepSys = lepSys + lep1.transFourVect();
  }
  L->SetLabFrameFourVector(lepSys);

  INV->SetLabFrameThreeVector(metVec.Vect());
  if (!LAB->AnalyzeEvent()) std::cout << "Something went wrong..." << std::endl;

  // Compressed variables from tree
  int Njet_V = 0;
  int Njet_ISR = 0;  
  for (unsigned int i = 0; i < signalJets.size(); ++i){
    if (VIS->GetFrame(jetID[i]) == *V) {
      Njet_V++;
    } else {
      Njet_ISR++;
    }
  }

  //RJR variables
  float Pt_ISR = 0.0; 
  float R_ISR = 0.0;
  float M_S = 0.0;
  float M_ISR = 0.0;
  float DPhi_I_ISR = 0.0;

  TVector3 v_P_ISR = ISR->GetFourVector(*CM).Vect();
  TVector3 v_P_I   = I->GetFourVector(*CM).Vect();
  Pt_ISR = v_P_ISR.Mag();
  R_ISR = fabs(v_P_I.Dot(v_P_ISR.Unit())) / Pt_ISR;
  M_S = S->GetMass();
  M_ISR = ISR->GetMass();
  DPhi_I_ISR = fabs(v_P_ISR.DeltaPhi(v_P_I));

  //---------------------------------------------------------
  //
  //                Calculate other variables
  //
  //---------------------------------------------------------

  // Events must have ==2 signal leptons or ==1 signal lepton plus >=1 track
  bool is2L  = (signalLeptons.size() == 2);
  bool is1LT = (signalLeptons.size() == 1 && signalTracks.size()>=1); 
  if ( !(is2L || is1LT) ) return;

  auto lep1 = signalLeptons[0];
  auto lep2 = is2L ? signalLeptons[1] : signalTracks[0];
  auto lep = lep1+lep2;
  
  float jet1Pt    = signalJets.size() >= 1 ? signalJets[0].Pt() : -1.;
  float dPhiJ1Met = signalJets.size() >= 1 ? fabs( signalJets[0].DeltaPhi( metVec ) ) : -1.; 

  // Minimum of the delta phi between all jets with MET
  float minDPhiAllJetsMet = minDphi(metVec,signalJets);

  float lep1Pt = lep1.Pt();
  float lep2Pt = lep2.Pt();

  float dPhiLepMet = fabs( lep.DeltaPhi( metVec ) );

  // SFOS decision
  bool isOS = ( lep1.charge() != lep2.charge() ); // opposite charge
  bool isSF = ( lep1.type() == lep2.type() ) ; // same flavour
  bool isee = ( lep1.type() == ELECTRON && lep2.type() == ELECTRON ); //di-electron
  bool ismm = ( lep1.type() == MUON && lep2.type() == MUON ); //di-muon

  // Higher level leptonic variables
  float mll          = (lep1+lep2).M();
  float drll         = lep1.DeltaR(lep2);
  float metOverHtLep = met / (lep1Pt + lep2Pt); 
  float MTauTau      = calcMTauTau(lep1, lep2, metVec);
  float mt           = calcMT( lep1,metVec );
  
  // mT2 with trial invisible particle mass at 100 GeV
  float mn = 100.; 
  //  float mt2 = calc_massive_MT2(baseLeptons[0], baseLeptons[1], metVec, mn);   
  float mt2=calcAMT2(lep1,lep2,metVec,mn,mn);

  // SUSYTools final state (fs) code
  int fs = event->getSUSYChannel();
  // stau veto for slepton signal - note this does not affect the denominator in the acceptance numbers (not needed in R21)
  //  bool isNotStau = ( fs != 206 && fs != 207 );

  int mcChannel = event->getMCNumber();

  //bool is1LTsample = (mcChannel >= 396160 && mcChannel <= 396205) || (mcChannel >= 397091 && mcChannel <= 397099);

  //---------------------------------------------------------
  //
  //                Perform selection
  //
  //---------------------------------------------------------

  bool pass_pre1LT_cuts = false;
  if (    ( is1LT )
       && ( lep1Pt < 10. ) 
       && ( drll > 0.05 && drll < 1.5 )
       && ( isOS && isSF )
       && ( mll > 0.5 && mll < 5 )
       && ( mll < 3. || mll > 3.2 )
       && ( met >  120. )
       && ( signalJets.size() >= 1 )
       && ( jet1Pt               >  100. )
       && ( minDPhiAllJetsMet    >  0.4 )
       && ( dPhiJ1Met            >  2.0 )
    ) pass_pre1LT_cuts = true;

  bool keep_SR_E_1LT = pass_pre1LT_cuts
                        && ( met >  200. )
                        && ( metOverHtLep >  30. )
                        && ( dPhiLepMet < 1. )
                        && ( lep2Pt < 5. );
  
  // Common 2L cuts
  bool pass_common_cuts = false;
  if (    ( is2L )
       && ( lep1Pt > 5. )
       && ( (drll > 0.05 && ismm) || (drll > 0.3 && isee) )
       && ( isOS && isSF )
       && ( (mll > 1. && mll < 60. && ismm) || (mll > 3. && mll < 60. && isee) )
       && ( mll < 3. || mll > 3.2 )
       && ( MTauTau < 0. || MTauTau > 160. )
       && ( met >  120. )
       && ( signalJets.size() >= 1 )
       && ( signalBJets.size()   == 0 ) 
       && ( jet1Pt               >  100. )
       && ( minDPhiAllJetsMet    >  0.4 )
       && ( dPhiJ1Met            >  2.0 )
      ) pass_common_cuts = true;

  bool keep_SR_E_high =  pass_common_cuts 
                              && ( met >  200. )
                              && ( mt < 60. )
                              && ( lep2Pt > std::min(10., 2 + mll/3.) )
                              && ( R_ISR < 1. )
                              && ( R_ISR > std::max(0.85, 0.98 - 0.02*mll) );

  bool keep_SR_E_med =  pass_common_cuts 
                                   && ( mll < 30. )
                                   && ( met > 120. )
                                   && ( met < 200. )
                                   && ( metOverHtLep > 10. )
                                   && ( M_S < 50. );

  bool keep_SR_E_low =  pass_common_cuts 
                                    && ( met > 120. )
                                    && ( met < 200. )
                                    && ( metOverHtLep < 10. )
                                    && ( lep2Pt > (5. + mll/4.) )
                                    && ( mt > 10. && mt < 60. )
                                    && ( R_ISR < 1. && R_ISR > 0.8 );

  bool keep_SR_E = keep_SR_E_high || keep_SR_E_med || keep_SR_E_low || keep_SR_E_1LT;

  bool keep_iMLLa = mll < 1.;
  bool keep_iMLLb = mll < 2.;
  bool keep_iMLLc = mll < 3.;
  bool keep_iMLLd = mll < 5.;
  bool keep_iMLLe = mll < 10.;
  bool keep_iMLLf = mll < 20.;
  bool keep_iMLLg = mll < 30.;
  bool keep_iMLLh = mll < 40.;
  bool keep_iMLLi = mll < 60.;

  bool keep_SR_S_high =  pass_common_cuts 
                              && ( met >  200. )
                              && ( mt2 < 140. && mt2 > 100. )
                              && ( lep2Pt > std::min(20., 2.5 + 2.5*(mt2 - 100.)) )
                              && ( R_ISR < 1. )
                              && ( R_ISR > std::max(0.85, 0.98 - 0.02*(mt2-100)) );

  bool keep_SR_S_low =  pass_common_cuts 
                                    && ( met > 120. )
                                    && ( met < 200. )
                                    && ( mt2 < 140. && mt2 > 100. )
                                    && ( lep2Pt > std::min(15., 7.5 + 0.75*(mt2 - 100.)) )
                                    && ( R_ISR < 1. && R_ISR > 0.8 );

  bool keep_SR_S = keep_SR_S_high || keep_SR_S_low;

  bool keep_iMT2a = mt2 < 100.5;
  bool keep_iMT2b = mt2 < 101.;
  bool keep_iMT2c = mt2 < 102.;
  bool keep_iMT2d = mt2 < 105.;
  bool keep_iMT2e = mt2 < 110.;
  bool keep_iMT2f = mt2 < 120.;
  bool keep_iMT2g = mt2 < 130.;
  bool keep_iMT2h = mt2 < 140.;                  

  //Electroweakino inclusive SRs
  //high
  if (keep_SR_E_high && keep_iMLLa) accept("SR_E_high_iMLLa"); //should be empty
  if (keep_SR_E_high && keep_iMLLb) accept("SR_E_high_iMLLb");
  if (keep_SR_E_high && keep_iMLLc) accept("SR_E_high_iMLLc");
  if (keep_SR_E_high && keep_iMLLd) accept("SR_E_high_iMLLd");
  if (keep_SR_E_high && keep_iMLLe) accept("SR_E_high_iMLLe");
  if (keep_SR_E_high && keep_iMLLf) accept("SR_E_high_iMLLf");
  if (keep_SR_E_high && keep_iMLLg) accept("SR_E_high_iMLLg");
  if (keep_SR_E_high && keep_iMLLh) accept("SR_E_high_iMLLh");
  if (keep_SR_E_high && keep_iMLLi) accept("SR_E_high_iMLLi");
  //med
  if (keep_SR_E_med && keep_iMLLa) accept("SR_E_med_iMLLa"); //should be empty
  if (keep_SR_E_med && keep_iMLLb) accept("SR_E_med_iMLLb");
  if (keep_SR_E_med && keep_iMLLc) accept("SR_E_med_iMLLc");
  if (keep_SR_E_med && keep_iMLLd) accept("SR_E_med_iMLLd");
  if (keep_SR_E_med && keep_iMLLe) accept("SR_E_med_iMLLe");
  if (keep_SR_E_med && keep_iMLLf) accept("SR_E_med_iMLLf");
  if (keep_SR_E_med && keep_iMLLg) accept("SR_E_med_iMLLg");
  if (keep_SR_E_med && keep_iMLLh) accept("SR_E_med_iMLLh"); //should be the same as iMLLg
  if (keep_SR_E_med && keep_iMLLi) accept("SR_E_med_iMLLi"); //should be the same as iMLLg
  //low
  if (keep_SR_E_low && keep_iMLLa) accept("SR_E_low_iMLLa"); //should be empty
  if (keep_SR_E_low && keep_iMLLb) accept("SR_E_low_iMLLb");
  if (keep_SR_E_low && keep_iMLLc) accept("SR_E_low_iMLLc");
  if (keep_SR_E_low && keep_iMLLd) accept("SR_E_low_iMLLd");
  if (keep_SR_E_low && keep_iMLLe) accept("SR_E_low_iMLLe");
  if (keep_SR_E_low && keep_iMLLf) accept("SR_E_low_iMLLf");
  if (keep_SR_E_low && keep_iMLLg) accept("SR_E_low_iMLLg");
  if (keep_SR_E_low && keep_iMLLh) accept("SR_E_low_iMLLh");
  if (keep_SR_E_low && keep_iMLLi) accept("SR_E_low_iMLLi");
  //1LT
  if (keep_SR_E_1LT && keep_iMLLa) accept("SR_E_lT_iMLLa"); //should *not* be empty
  if (keep_SR_E_1LT && keep_iMLLb) accept("SR_E_lT_iMLLb");
  if (keep_SR_E_1LT && keep_iMLLc) accept("SR_E_lT_iMLLc");
  if (keep_SR_E_1LT && keep_iMLLd) accept("SR_E_lT_iMLLd");
  if (keep_SR_E_1LT && keep_iMLLe) accept("SR_E_lT_iMLLe"); //should be same as iMLLd
  if (keep_SR_E_1LT && keep_iMLLf) accept("SR_E_lT_iMLLf"); //should be same as iMLLd
  if (keep_SR_E_1LT && keep_iMLLg) accept("SR_E_lT_iMLLg"); //should be same as iMLLd
  if (keep_SR_E_1LT && keep_iMLLh) accept("SR_E_lT_iMLLh"); //should be same as iMLLd
  if (keep_SR_E_1LT && keep_iMLLi) accept("SR_E_lT_iMLLi"); //should be same as iMLLd
  //OR
  if (keep_SR_E && keep_iMLLa) accept("SR_E_iMLLa");
  if (keep_SR_E && keep_iMLLb) accept("SR_E_iMLLb");
  if (keep_SR_E && keep_iMLLc) accept("SR_E_iMLLc");
  if (keep_SR_E && keep_iMLLd) accept("SR_E_iMLLd");
  if (keep_SR_E && keep_iMLLe) accept("SR_E_iMLLe");
  if (keep_SR_E && keep_iMLLf) accept("SR_E_iMLLf");
  if (keep_SR_E && keep_iMLLg) accept("SR_E_iMLLg");
  if (keep_SR_E && keep_iMLLh) accept("SR_E_iMLLh");
  if (keep_SR_E && keep_iMLLi) accept("SR_E_iMLLi");

  //Electroweakino exclusive SRs
  //high
  if (keep_SR_E_high && mll < 2.)       accept("SR_E_high_eMLLa");
  else if (keep_SR_E_high && mll < 3.)  accept("SR_E_high_eMLLb");
  else if (keep_SR_E_high && mll < 5.)  accept("SR_E_high_eMLLc");
  else if (keep_SR_E_high && mll < 10.) accept("SR_E_high_eMLLd");
  else if (keep_SR_E_high && mll < 20.) accept("SR_E_high_eMLLe");
  else if (keep_SR_E_high && mll < 30.) accept("SR_E_high_eMLLf");
  else if (keep_SR_E_high && mll < 40.) accept("SR_E_high_eMLLg");
  else if (keep_SR_E_high && mll < 60.) accept("SR_E_high_eMLLh");
  //med
  if (keep_SR_E_med && mll < 2.)       accept("SR_E_med_eMLLa");
  else if (keep_SR_E_med && mll < 3.)  accept("SR_E_med_eMLLb");
  else if (keep_SR_E_med && mll < 5.)  accept("SR_E_med_eMLLc");
  else if (keep_SR_E_med && mll < 10.) accept("SR_E_med_eMLLd");
  else if (keep_SR_E_med && mll < 20.) accept("SR_E_med_eMLLe");
  else if (keep_SR_E_med && mll < 30.) accept("SR_E_med_eMLLf");
  //low
  if (keep_SR_E_low && mll < 2.)       accept("SR_E_low_eMLLa");
  else if (keep_SR_E_low && mll < 3.)  accept("SR_E_low_eMLLb");
  else if (keep_SR_E_low && mll < 5.)  accept("SR_E_low_eMLLc");
  else if (keep_SR_E_low && mll < 10.) accept("SR_E_low_eMLLd");
  else if (keep_SR_E_low && mll < 20.) accept("SR_E_low_eMLLe");
  else if (keep_SR_E_low && mll < 30.) accept("SR_E_low_eMLLf");
  else if (keep_SR_E_low && mll < 40.) accept("SR_E_low_eMLLg");
  else if (keep_SR_E_low && mll < 60.) accept("SR_E_low_eMLLh");
  //1L1T (0.5, 1, 1.5, 2, 3, 4, and 5 GeV)
  if (keep_SR_E_1LT && mll < 1.)       accept("SR_E_lT_eMLLa");
  else if (keep_SR_E_1LT && mll < 1.5) accept("SR_E_lT_eMLLb");
  else if (keep_SR_E_1LT && mll < 2.)  accept("SR_E_lT_eMLLc");
  else if (keep_SR_E_1LT && mll < 3.)  accept("SR_E_lT_eMLLd");
  else if (keep_SR_E_1LT && mll < 4.)  accept("SR_E_lT_eMLLe");
  else if (keep_SR_E_1LT && mll < 5.)  accept("SR_E_lT_eMLLf");

  //Slepton inclusive SRs
  //high
  if (keep_SR_S_high && keep_iMT2a) accept("SR_S_high_iMT2a");
  if (keep_SR_S_high && keep_iMT2b) accept("SR_S_high_iMT2b");
  if (keep_SR_S_high && keep_iMT2c) accept("SR_S_high_iMT2c");
  if (keep_SR_S_high && keep_iMT2d) accept("SR_S_high_iMT2d");
  if (keep_SR_S_high && keep_iMT2e) accept("SR_S_high_iMT2e");
  if (keep_SR_S_high && keep_iMT2f) accept("SR_S_high_iMT2f");
  if (keep_SR_S_high && keep_iMT2g) accept("SR_S_high_iMT2g");
  if (keep_SR_S_high && keep_iMT2h) accept("SR_S_high_iMT2h");
  //low
  if (keep_SR_S_low && keep_iMT2a) accept("SR_S_low_iMT2a");
  if (keep_SR_S_low && keep_iMT2b) accept("SR_S_low_iMT2b");
  if (keep_SR_S_low && keep_iMT2c) accept("SR_S_low_iMT2c");
  if (keep_SR_S_low && keep_iMT2d) accept("SR_S_low_iMT2d");
  if (keep_SR_S_low && keep_iMT2e) accept("SR_S_low_iMT2e");
  if (keep_SR_S_low && keep_iMT2f) accept("SR_S_low_iMT2f");
  if (keep_SR_S_low && keep_iMT2g) accept("SR_S_low_iMT2g");
  if (keep_SR_S_low && keep_iMT2h) accept("SR_S_low_iMT2h");
  //OR
  if (keep_SR_S && keep_iMT2a) accept("SR_S_iMT2a");
  if (keep_SR_S && keep_iMT2b) accept("SR_S_iMT2b");
  if (keep_SR_S && keep_iMT2c) accept("SR_S_iMT2c");
  if (keep_SR_S && keep_iMT2d) accept("SR_S_iMT2d");
  if (keep_SR_S && keep_iMT2e) accept("SR_S_iMT2e");
  if (keep_SR_S && keep_iMT2f) accept("SR_S_iMT2f");
  if (keep_SR_S && keep_iMT2g) accept("SR_S_iMT2g");
  if (keep_SR_S && keep_iMT2h) accept("SR_S_iMT2h");

  //Slepton exclusive SRs
  //high
  if (keep_SR_S_high && mt2 < 100.5)     accept("SR_S_high_eMT2a");
  else if (keep_SR_S_high && mt2 < 101.) accept("SR_S_high_eMT2b");
  else if (keep_SR_S_high && mt2 < 102.) accept("SR_S_high_eMT2c");
  else if (keep_SR_S_high && mt2 < 105.) accept("SR_S_high_eMT2d");
  else if (keep_SR_S_high && mt2 < 110.) accept("SR_S_high_eMT2e");
  else if (keep_SR_S_high && mt2 < 120.) accept("SR_S_high_eMT2f");
  else if (keep_SR_S_high && mt2 < 130.) accept("SR_S_high_eMT2g");
  else if (keep_SR_S_high && mt2 < 140.) accept("SR_S_high_eMT2h");
  //low
  if (keep_SR_S_low && mt2 < 100.5)     accept("SR_S_low_eMT2a");
  else if (keep_SR_S_low && mt2 < 101.) accept("SR_S_low_eMT2b");
  else if (keep_SR_S_low && mt2 < 102.) accept("SR_S_low_eMT2c");
  else if (keep_SR_S_low && mt2 < 105.) accept("SR_S_low_eMT2d");
  else if (keep_SR_S_low && mt2 < 110.) accept("SR_S_low_eMT2e");
  else if (keep_SR_S_low && mt2 < 120.) accept("SR_S_low_eMT2f");
  else if (keep_SR_S_low && mt2 < 130.) accept("SR_S_low_eMT2g");
  else if (keep_SR_S_low && mt2 < 140.) accept("SR_S_low_eMT2h");

  // Fill in optional ntuple variables
  ntupVar("FS",                fs);               
  ntupVar("DSID",              mcChannel);
  ntupVar("mc_weight",  event->getMCWeights()[0]);  
  ntupVar("susyChannel",event->getSUSYChannel());
  ntupVar("mcWeights",event->getMCWeights());
  ntupVar("pdf_id1",event->getPDF_id1());
  ntupVar("pdf_x1",event->getPDF_x1());
  ntupVar("pdf_pdf1",event->getPDF_pdf1());
  ntupVar("pdf_id2",event->getPDF_id2());
  ntupVar("pdf_x2",event->getPDF_x2());
  ntupVar("pdf_pdf2",event->getPDF_pdf2());
  ntupVar("pdf_scale",event->getPDF_scale());
  ntupVar("met_Et",            met);               
  ntupVar("nLep_base",         static_cast<int>(baseLeptons.size()));               
  ntupVar("nLep_signal",       static_cast<int>(signalLeptons.size()));
  ntupVar("nEle_signal",       static_cast<int>(signalElectrons.size()));
  ntupVar("nMu_signal",        static_cast<int>(signalMuons.size()));         
  ntupVar("nBJet20_MV2c10",    static_cast<int>(signalBJets.size()));
  ntupVar("nJet30",            static_cast<int>(signalJets.size()));
  ntupVar("nTrk_signal",       static_cast<int>(signalTracks.size()));
  ntupVar("isOS",              isOS);               
  ntupVar("isSF",              isSF);
  ntupVar("isee",              isee);               
  ntupVar("ismm",              ismm);
  ntupVar("is2L",              is2L);
  ntupVar("is1LT",             is1LT);
  //ntupVar("is1LTsample",       is1LTsample);               
  ntupVar("DPhiJ1Met",         dPhiJ1Met);               
  ntupVar("lep1Pt",            lep1Pt);               
  ntupVar("lep2Pt",            lep2Pt);               
  ntupVar("jet1Pt",            jet1Pt);               
  ntupVar("mll",               mll);               
  ntupVar("Rll",               drll);               
  ntupVar("mt_lep1",           mt);               
  ntupVar("mt2leplsp_100",     mt2);               
  ntupVar("MTauTau",           MTauTau);               
  ntupVar("METOverHTLep",      metOverHtLep);               
  ntupVar("minDPhiAllJetsMet", minDPhiAllJetsMet);
  ntupVar("dPhiLepMet",        dPhiLepMet);
  ntupVar("RJR_PTISR",         Pt_ISR);
  ntupVar("RJR_RISR",          R_ISR);
  ntupVar("RJR_MS",            M_S);
  ntupVar("RJR_MISR",          M_ISR);
  ntupVar("RJR_dphiISRI",      DPhi_I_ISR);
  ntupVar("RJR_NjV",           Njet_V);
  ntupVar("RJR_NjISR",         Njet_ISR);
  ntupVar("pass_common_cuts",  pass_common_cuts);
  ntupVar("pass_pre1LT_cuts",  pass_pre1LT_cuts);
  ntupVar("pass_SR_E_high",    keep_SR_E_high);
  ntupVar("pass_SR_E_med",     keep_SR_E_med);
  ntupVar("pass_SR_E_low",     keep_SR_E_low);
  ntupVar("keep_SR_E_1LT",     keep_SR_E_1LT);
  ntupVar("pass_SR_E",         keep_SR_E);
  ntupVar("pass_SR_S_high",    keep_SR_S_high);
  ntupVar("pass_SR_S_low",     keep_SR_S_low);
  ntupVar("pass_SR_S",         keep_SR_S);
}

