#include "SimpleAnalysisFramework/AnalysisClass.h"
DefineAnalysis(EwkThreeLepton2016)

void EwkThreeLepton2016::Init()
{
  // Define signal/control regions
  addRegions({"WZ0Ja","WZ0Jb","WZ0Jc","WZ1Ja","WZ1Jb","WZ1Jc"});
  addRegions({"slepa","slepb","slepc","slepd","slepe"});
}

void EwkThreeLepton2016::ProcessEvent(AnalysisEvent *event)
{
  // Retrieve basic object lists   
  // PLEASE NOTE UNITS ARE ALL IN GEV, AND NOT ATLAS STANDARD MEV!!!    
  //get baseline objects                                      
  auto baselineElectrons  = event->getElectrons(10, 2.47, ELooseBLLH); // Filter on pT, eta and "ID" 
  auto baselineMuons       = event->getMuons(10, 2.4, MuMedium|MuNotCosmic|MuQoPSignificance);
  auto baselineJets       = event->getJets(20., 4.5);
  auto metVec             = event->getMET();
  double met              = metVec.Et();

  
  //overlap removal
  //1. Any electron sharing an ID track with a muon is removed 
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineMuons, 0.01);
  
  //2. if the jet within DR = 0.2 of the electron is not b-tagged, the jet itself is discarded, as it likely originates from an electron-induced shower.
  baselineJets      = overlapRemoval(baselineJets, baselineElectrons, 0.2, NOT(BTag77MV2c20));
  //2. If a b-tagged jet is within DR = 0.2 of an electron candidate, the electron is rejected, as it is likely to be from a semileptonic b-hadron decay
  baselineElectrons = overlapRemoval(baselineElectrons, baselineJets, 0.2);
  
  //3. Electrons within DR = 0.4 of a jet candidate are discarded,
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineJets, 0.4);

  //4. Jets with fewer than three associated tracks are discarded
  //5.Jets with a nearby muon that carries a significant fraction of the transverse momentum of the jet (pμ > 0.7   pjet tracks, where pμ and pjet tracks are the transverse momenta of the muon and the tracks TTTT associated with the jet, respectively) are discarded either if the candidate muon is within ∆R = 0.2 or if the muon is matched to a track associated with the jet.
  auto muJetSpecial = [] (const AnalysisObject& jet, const AnalysisObject& muon) {
    if (jet.pass(NOT(BTag77MV2c20)) && (jet.pass(LessThan3Tracks) || muon.Pt()/jet.Pt()>0.7)) return 0.2;
    else return 0.;
  };
  baselineJets = overlapRemoval(baselineJets, baselineMuons, muJetSpecial, NOT(BTag77MV2c20));

  //6. Muons within DR = 0.4 of a jet candidate are discarded to suppress muons from semileptonic decays of c- and b-hadrons.
  baselineMuons      = overlapRemoval(baselineMuons, baselineJets, 0.4);


  auto baselineLeptons   = baselineElectrons + baselineMuons;
  int nBaselineLeptons = baselineLeptons.size();

  //signal objects
  auto signalJets      = filterObjects(baselineJets, 20, 2.4, JVT50Jet);
  auto signalElectrons = filterObjects(baselineElectrons, 20, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signalMuons     = filterObjects(baselineMuons, 20, 2.4, MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
  auto bJets = filterObjects(signalJets, 30., 2.5, BTag77MV2c10);
  auto signalLeptons = signalElectrons+signalMuons;
  
  int nJets = signalJets.size();
  int nBjets = bJets.size();
  int nLeptons = signalLeptons.size();
  int nElectrons = signalElectrons.size();
  int nMuons = signalMuons.size();

  //pT order the objects
  sortObjectsByPt(signalLeptons);
  sortObjectsByPt(signalJets);

  //b-jet veto
  if(nBjets>0) return;

  //start preselecting 
  //exactly 3 baseline and signal leptons  
  if(nBaselineLeptons != 3) return;
  if(nLeptons != 3) return;

  //lepton pT cuts 
  if( (signalLeptons[0].Pt()<25) && (signalLeptons[1].Pt()<25) )return;

  //do assignment by minMT  
  float minMT = 999.;
  float mll_minMT = -999.;
  int nb_SFOS = 0;
  int leadZindex = -1;
  int subZindex = -1;
  int Windex = -1;

  for(int ilep=0; ilep<nLeptons-1; ilep++){
    for(int jlep=ilep+1; jlep<nLeptons; jlep++){
      int klep = nLeptons - ilep - jlep;
      if( (signalLeptons[ilep].type() == signalLeptons[jlep].type()) && (signalLeptons[ilep].charge() != signalLeptons[jlep].charge()) ){
        nb_SFOS++;

        float tmp_mll = (signalLeptons[ilep] + signalLeptons[jlep]).M();
        float tmp_mt = calcMT(signalLeptons[klep], metVec);
	if (tmp_mt < minMT){
	  minMT = tmp_mt;
          mll_minMT = tmp_mll;
          leadZindex = ilep;
          subZindex = jlep;
          Windex = klep;
        }
      }
    }
  }

  //remove events without SFOS 
  if(nb_SFOS==0) return;
  
  //at this point everything should be assigned! 
  if (leadZindex == -1 || subZindex == -1 || Windex == -1){
    std::cout << "this is waaay too weird Joana!" << std::endl;
  }

  //require the Zpeak
  float Zmass = 91.188;
  if(fabs(mll_minMT - Zmass)> 10.) return;

  float ptlll = (signalLeptons[0]+signalLeptons[1]+signalLeptons[2]).Pt();
  float mlll = (signalLeptons[0]+signalLeptons[1]+signalLeptons[2]).M();
  
  if (mll_minMT < 20.0) return;
  if (mlll < 20.0) return;

  if(mll_minMT < 81.2   &&   met > 130   &&   signalLeptons[2].Pt() > 20   &&   signalLeptons[2].Pt() < 30   &&   minMT>110) accept("slepa");
  if(mll_minMT < 81.2   &&   met > 130   &&   signalLeptons[2].Pt() > 30                                     &&   minMT>110) accept("slepb");

  if(mll_minMT > 101.2  &&   met > 130   &&   signalLeptons[2].Pt() > 20   &&   signalLeptons[2].Pt() < 50   &&   minMT>110) accept("slepc");
  if(mll_minMT > 101.2  &&   met > 130   &&   signalLeptons[2].Pt() > 50   &&   signalLeptons[2].Pt() < 80   &&   minMT>110) accept("slepd");
  if(mll_minMT > 101.2  &&   met > 130   &&   signalLeptons[2].Pt() > 80                                     &&   minMT>110) accept("slepe");

  if(mll_minMT > 81.2 && mll_minMT < 101.2 && nJets==0 && met>60  && met<120 && minMT>110) accept("WZ0Ja");
  if(mll_minMT > 81.2 && mll_minMT < 101.2 && nJets==0 && met>120 && met<170 && minMT>110) accept("WZ0Jb");
  if(mll_minMT > 81.2 && mll_minMT < 101.2 && nJets==0 && met>170            && minMT>110) accept("WZ0Jc");

  if(mll_minMT > 81.2 && mll_minMT < 101.2 && nJets>0 && met>120 && met<200 && minMT>110 && signalJets[0].Pt()>70 && ptlll<120) accept("WZ1Ja");
  if(mll_minMT > 81.2 && mll_minMT < 101.2 && nJets>0 && met>200            && minMT>110 && minMT<160) accept("WZ1Jb");
  if(mll_minMT > 81.2 && mll_minMT < 101.2 && nJets>0 && met>200            && minMT>160 && signalLeptons[2].Pt()>35) accept("WZ1Jc");

  ntupVar("met",met);
  ntupVar("mll_minMT",mll_minMT);
  ntupVar("minMT",minMT);
  ntupVar("nJets",nJets);
  ntupVar("nBjets",nBjets);
  ntupVar("nLeptons",nLeptons);
  ntupVar("nElectrons",nElectrons);
  ntupVar("nMuons",nMuons);
  ntupVar("firstLepton",signalLeptons[0]);
  ntupVar("secondLepton",signalLeptons[1]);
  ntupVar("thirdLepton",signalLeptons[2]);
  ntupVar("Zlep1",signalLeptons[leadZindex]);
  ntupVar("Zlep2",signalLeptons[subZindex]);
  ntupVar("Wlep",signalLeptons[Windex]);
  ntupVar("ptlll",ptlll);
  if (nJets>0)
    ntupVar("leadJet",signalJets[0]);
  ntupVar("susyProcess",event->getSUSYChannel());
  ntupVar("mcDSID",event->getMCNumber());
  ntupVar("mcWeights",event->getMCWeights());
  return;
}
