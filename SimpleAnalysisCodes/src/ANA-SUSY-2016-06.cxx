#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <TRandom.h>
#include <TFile.h>
#include <TH2.h>
#include <TF1.h>

DefineAnalysis(DisappearingTrack2016)

namespace
{
  class effMap {
  public:
    effMap(const char* name) {
      TFile fh(FindFile("DisappearingTrack2016-TrackAcceptanceEfficiency.root").c_str());
      effHist = dynamic_cast < TH2 * >(fh.FindObjectAny(name)->Clone());
      effHist->SetDirectory(0);
      if (!effHist) throw std::runtime_error("Could not find efficiency map for DisappearingTrack2016");
    };
    ~effMap() { delete effHist; };
    double eff(double eta, double radius) {
      return effHist->GetBinContent(effHist->FindBin(eta,radius));
    };
  private:
    TH2* effHist;
  };
    // Efficiency maps depend on the production channel.
    // Strong channel has lower acceptance and efficiency
    effMap *acceffStrong        = 0;
    effMap *acceffElectroweak   = 0;
    TF1 *fgFDQOverPt            = 0;

    // Switch to simulated pT resolution of tracklets
    const bool doPtSmearing = true;

    // Smearing function to simulation pT resolution
    double PixelTrackletSmearingFunction(double *x, double */*par*/) {
        double constant = 1;                             // constant    
        double mean     = -1.72142e+00;                  // mean  [/TeV]
        double sigma    = 1.32009e+01 * (1 - 0.0843956); // sigma [/TeV]
        double alpha    = 1.66707e+00 * (1 - 0.0498447); // slope       

        // evaluate the crystal ball function
        double z = (x[0] - mean) / sigma;
        alpha = fabs(alpha);
        double norm1 = sigma * sqrt(2 * M_PI) * erf(alpha / sqrt(2));
        double norm2 = sigma * exp(-alpha * alpha / 2) / alpha;
        double norm3 = norm2;
        constant /= (norm1 + norm2 + norm3);
        if (z < -alpha) {
            return constant * std::exp(+alpha * (z + 0.5 * alpha));
        } else if (z > +alpha) {
            return constant * std::exp(-alpha * (z - 0.5 * alpha));
        } else {
            return constant * std::exp(-0.5 * z * z);
        }
    }
}

void DisappearingTrack2016::Init()
{
    // Define signal/control regions
    addRegion("SR_Electroweak");
    addRegion("SR_Strong");

    // Read efficiency map
    acceffStrong      = new effMap("StrongEfficiency");
    acceffElectroweak = new effMap("ElectroweakEfficiency");

    // Define tracklet q/pT smearing function
    fgFDQOverPt = new TF1("SmearingFunction",
                               [&](double *x, double *p) { return PixelTrackletSmearingFunction(x, p); },
                               -1000, 1000, 0);

    // Book 1/2D histograms
    const Int_t ncut = 8;
    addHistogram("cutFlow", ncut, -0.5, ncut - 0.5);
}

void DisappearingTrack2016::ProcessEvent(AnalysisEvent * event)
{
    // Retrieve basic object lists
    // PLEASE NOTE UNITS ARE ALL IN GEV
    auto   electrons      = event->getElectrons   ( 10 , 2.7   , ELooseLH);     // Filter on pT , eta and "ID"
    auto   muons          = event->getMuons       ( 10 , 2.47  , MuMedium);
    auto   jets           = event->getJets        ( 20 , 2.8);
    auto   jets50         = event->getJets        ( 50 , 2.8);
    auto   charginos      = event->getHSTruth(20, 1.9, Chargino1|StablePart);
    auto   Allcharginos   = event->getHSTruth(0 , 5.0, Chargino1|StablePart);
    auto   metVec         = event->getMET();

    sortObjectsByPt(charginos);
    sortObjectsByPt(Allcharginos);

    Int_t cutFlowStep = 0;
    static int entry =0;
    entry++;
    fill("cutFlow", cutFlowStep++);
    

    // Reject events with bad jets
    if (countObjects(jets, 50, 2.8, NOT(LooseBadJet)) != 0) {
        return;
    }

    fill("cutFlow", cutFlowStep++);

    // Standard SUSY overlap removal
    jets      = overlapRemoval(jets      , Allcharginos , 0.05);
    jets      = overlapRemoval(jets      , electrons    , 0.2);
    electrons = overlapRemoval(electrons , jets         , 0.4);
    muons     = overlapRemoval(muons     , jets         , 0.4);
    jets50    = overlapRemoval(jets50    , Allcharginos , 0.05);

    // Lepton veto
    if (!electrons.empty() || !muons.empty()) {
        return;
    }

    fill("cutFlow"   , cutFlowStep++);

    // Overlap removal between signal candidate and jets
    charginos = overlapRemoval(charginos, jets50, 0.4);

    double met = metVec.Pt();

    AnalysisObjects EWTracklets;
    AnalysisObjects StrongTracklets;
    // Simulate tracklet efficiency
    for (const auto& chargino : charginos) {
        TVector3 decayPosition=chargino.decayVtx();

	double rnd = gRandom->Rndm(); //FIXME: no way of setting random seed
	auto tracklet=chargino;
	if (doPtSmearing) {
	  double qoverpt =tracklet.charge() / tracklet.Pt() * (1e+3); // GeV -> TeV
	  double qoverptSmeared = qoverpt + fgFDQOverPt->GetRandom();
	  double ptSmeared = fabs(1. / qoverptSmeared) * (1e+3); // /TeV -> GeV
	  tracklet.SetPerp(ptSmeared);
	}
	if (rnd<=acceffElectroweak->eff(decayPosition.PseudoRapidity(), decayPosition.Perp()))
	  EWTracklets.push_back(tracklet);
	if (rnd<=acceffStrong->eff(decayPosition.PseudoRapidity(), decayPosition.Perp()))
	  StrongTracklets.push_back(tracklet);
    }
    sortObjectsByPt(EWTracklets);
    sortObjectsByPt(StrongTracklets);

    float dphiMin = minDphi(metVec, jets50, 4);

    // Electroweak
    bool accepted = true;

    if (accepted && jets.empty()) {
      accepted = false;
    }
    if (accepted) {
      fill("cutFlow", cutFlowStep++);
    }
    if (accepted && fabs(jets[0].Eta()) >  2.4) { accepted = false; }                // Leading jet phi
    if (accepted && jets[0].Pt()        <= 140) { accepted = false; }                // Leading jet pT
    if (accepted)                               { fill("cutFlow", cutFlowStep++); }
    if (accepted && met                 <= 140) { accepted = false; }                // MET
    if (accepted)                               { fill("cutFlow", cutFlowStep++); }
    if (accepted && dphiMin             <= 1)   { accepted = false; }                // dPhi(MET, jets)
    if (accepted)                               { fill("cutFlow", cutFlowStep++); }
    if (accepted) {
      fill("cutFlow", cutFlowStep++);
      if (EWTracklets.size()>0 && EWTracklets[0].Pt()>100) accept("SR_Electroweak");
    }

    // Strong
    accepted = true;
    if (accepted && jets.size()  <= 2)   { accepted = false; } // Number of jets
    if (accepted && jets[0].Pt() <= 100) { accepted = false; } // 1st jet pT
    if (accepted && jets[1].Pt() <= 50)  { accepted = false; } // 2nd jet pT
    if (accepted && jets[2].Pt() <= 50)  { accepted = false; } // 3rd jet pT
    if (accepted && met          <= 150) { accepted = false; } // MET
    if (accepted && dphiMin      <= 0.4) { accepted = false; } // dPhi(MET, jets)
    if (accepted) {
      if (StrongTracklets.size()>0 && StrongTracklets[0].Pt()>100) accept("SR_Strong");
    }

    return;
}
