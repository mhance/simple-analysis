#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(Diphoton2018)

void Diphoton2018::Init()
{
  addRegions({"Cat0","Cat1","Cat2","Cat3","Cat4","Cat5","Cat6","Cat7","Cat8","Cat9","Cat10","Cat11","Cat12"});
}

void Diphoton2018::ProcessEvent(AnalysisEvent *event)
{
  // Baseline objects
  auto photons   = event->getPhotons(25., 2.37, PhotonGood);
  auto electrons = event->getElectrons(20., 2.47, EMediumLH);
  auto muons     = event->getMuons(20., 2.7, MuMedium);
  auto jets      = event->getJets(25., 4.4);
  auto met       = event->getMET();
  double sumet   = event->getSumET();
  // Remove events with one bad jet
  if (countObjects(jets, 25, 4.4, NOT(LooseBadJet)) != 0) return;

  // Overlap removal
  photons    = overlapRemoval(photons, electrons, 0.01);
  jets 	     = overlapRemoval(jets, photons, 0.4);
  jets       = overlapRemoval(jets, electrons, 0.4);
  jets       = overlapRemoval(jets, muons, 0.4);
  // Signal objects
  auto signal_photons   = filterObjects(photons,   27., 2.37, PhotonIsoGood);
  auto signal_muons     = filterObjects(muons,     20., 2.7,  MuD0Sigma3|MuZ05mm|MuIsoGradientLoose);
  auto signal_electrons = filterObjects(electrons, 20., 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signal_jets      = filterObjects(jets,      25., 4.4);

  signal_photons   = filterCrack(signal_photons);
  signal_electrons = filterCrack(signal_electrons);

  sortObjectsByPt(signal_photons);
  sortObjectsByPt(signal_jets);

  // At least two photons with pt>75 GeV
  if (signal_photons.size() < 2)
    return;
  if (signal_photons[0].Pt() < 125*0.35) return;
  if (signal_photons[1].Pt() < 125*0.25) return;

  int n_leptons=signal_muons.size()+signal_electrons.size();
  int n_jets=signal_jets.size();
  bool gen_lepton_pass_ptcut=true;//we select 20GeV leptons already.
  double gen_mjj=0;
  if(signal_jets.size()>=2)gen_mjj=(signal_jets.at(0)+signal_jets.at(1)).M();
  bool gen_mjj_inrange = gen_mjj>40. and gen_mjj<120.;
  double gen_metsig=met.Et()/sqrt(sumet);
  accept("Cat0");

  if (n_leptons >= 1 and gen_lepton_pass_ptcut and gen_metsig>0 and gen_metsig<=2) accept("Cat1");
  if (n_leptons >= 1 and gen_lepton_pass_ptcut and gen_metsig>2 and gen_metsig<=4) accept("Cat2");
  if (n_leptons >= 1 and gen_lepton_pass_ptcut and gen_metsig>4 and gen_metsig<=6) accept("Cat3");
  if (n_leptons >= 1 and gen_lepton_pass_ptcut and gen_metsig>6) accept("Cat4");
  if (n_leptons == 0 and n_jets >= 2 and gen_mjj_inrange and gen_metsig>5 and gen_metsig<=6) accept("Cat5");
  if (n_leptons == 0 and n_jets >= 2 and gen_mjj_inrange and gen_metsig>6 and gen_metsig<=7) accept("Cat6");
  if (n_leptons == 0 and n_jets >= 2 and gen_mjj_inrange and gen_metsig>7 and gen_metsig<=8) accept("Cat7");
  if (n_leptons == 0 and n_jets >= 2 and gen_mjj_inrange and gen_metsig>8) accept("Cat8");
  if (n_leptons == 0 and (n_jets<2 or (n_jets>=2 and not gen_mjj_inrange)) and gen_metsig>6 and gen_metsig<=7) accept("Cat9");
  if (n_leptons == 0 and (n_jets<2 or (n_jets>=2 and not gen_mjj_inrange)) and gen_metsig>7 and gen_metsig<=8) accept("Cat10");
  if (n_leptons == 0 and (n_jets<2 or (n_jets>=2 and not gen_mjj_inrange)) and gen_metsig>8 and gen_metsig<=9) accept("Cat11");
  if (n_leptons == 0 and (n_jets<2 or (n_jets>=2 and not gen_mjj_inrange)) and gen_metsig>9) accept("Cat12");

  return;
}
