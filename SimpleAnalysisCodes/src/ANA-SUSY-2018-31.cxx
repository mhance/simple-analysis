#include "SimpleAnalysisFramework/AnalysisClass.h"
DefineAnalysis(SbottomMultiB2018)

//Author: H. Teagle


static bool maxmin_alg(double &maxDR, double &maxminDR, double &max_M, double &maxmin_M, double &max_avg_M, AnalysisObjects bJets);
void SbottomMultiB2018::Init()

{
  // Define signal/control regions
  addRegions({"SRA_incl","SRA_L","SRA_M","SRA_H"});
  addRegion("SRB");
  addRegions({"SRC_incl","SRC_22","SRC_24","SRC_26","SRC_28"});
  addRegion("preSelection");
  addHistogram("h_SumOfWeights",1,0.5,1.5);
}

void SbottomMultiB2018::ProcessEvent(AnalysisEvent *event)
{
  fill("h_SumOfWeights", event->getMCWeights()[0]);
  //
  // Soft Objects
  //
  auto  preOR_base_el = event->getElectrons(4.5,2.47,ELooseBLLH);
  auto  preOR_base_mu = event->getMuons(4,2.70, MuNotCosmic & MuMedium);
  auto  preOR_base_jet = event->getJets(20.,2.8);
  auto  ETMiss_Vect  = event->getMET();
  double ETMiss = ETMiss_Vect.Pt();
  
  auto base_jets = overlapRemoval(preOR_base_jet,preOR_base_el,0.2);
  auto base_el = overlapRemoval(preOR_base_el,base_jets,0.4); 
  auto base_mu = overlapRemoval(preOR_base_mu,base_jets,0.4);

  auto jets = filterObjects(base_jets, 30, 2.8);
  auto nonbJets = filterObjects(base_jets, 30, 2.8, NOT(BTag77MV2c10));
  auto bJets = filterObjects(base_jets,30,2.5,BTag77MV2c10);
  auto electrons = filterObjects(base_el, 20, 2.47,EIsoFixedCutTight);
  auto muons = filterObjects(base_mu, 20, 2.7,EIsoFixedCutTightTrackOnly);

  int mcChannel = event->getMCNumber();  
  int nBaselineLeptons =0;
  int nJets =0;
  int nbJets = 0;
  bool nonBLead = false; 
  
  if(nonbJets.size()>0){
    if (nbJets>0 && (nonbJets[0].Pt()>bJets[0].Pt())) {
      nonBLead = true;
      }
      else nonBLead = false;
  }
  
  nBaselineLeptons= base_mu.size()+base_el.size();
  nJets = jets.size();
  nbJets = bJets.size();

  //minDPhis
  double minDPhiJMET_4 = minDphi(ETMiss_Vect,jets,4);
  double DPhiJMET_1 = minDphi(ETMiss_Vect,jets,1);
  
  // Preselection
  //
  if(nBaselineLeptons ==0 && nbJets >=3 && ETMiss>250 && minDPhiJMET_4>0.4 ){
    accept("preSelection");
  }
  //else return;//preselection common to the signal regions for quick running
  //Calculate everything complicated if you pass preselection


  double maxDR = -1; 
  double maxminDR = -1;
  double maxmin_M = -99;
  double max_M =-99;
  double max_avg_M =-99;
  //Get the higgs id algorithm variables
  if(nbJets >3){
    if (!maxmin_alg(maxDR, maxminDR, max_M, maxmin_M, max_avg_M, bJets))
      {
	std::cout<<"Failed the maxmin algorithm method!!"<<std::endl;
	throw std::exception();
      }
  }
  
  //Effective mass 
  double HT = sumObjectsPt(jets);
  double meff = HT+ETMiss;

  double metsig_HT =-99;
  //METSig
  if (nJets>0){
    metsig_HT = ETMiss/sqrt(HT);
  }

  // //Filling jet vectors  
  double  pTj1=0, pTb1=0;
  if (nJets > 0) {
    pTj1 = jets[0].Pt();
  }

  // //Filling b-jet vectors  
  if (nbJets > 0) {
    pTb1 = bJets[0].Pt();
  }

  //Signal Regions
  bool SRA_common;
  if(nBaselineLeptons==0 && nJets>=6 && nbJets >=4 && maxDR>2.5 && maxminDR<2.5 && maxmin_M >80 && ETMiss >350 && pTb1 >200 && minDPhiJMET_4 >0.4) SRA_common =true; 
  else SRA_common = false; 

  if (SRA_common && meff>1000)              accept("SRA_incl");
  if (SRA_common && meff>1000 && meff<1500) accept("SRA_L");
  if (SRA_common && meff>1500 && meff<2000) accept("SRA_M");
  if (SRA_common && meff>2000)              accept("SRA_H");
  
  if (nBaselineLeptons==0 && ETMiss>350 && nJets>=4 && nbJets >=4 && (max_avg_M>75 && max_avg_M<175) && nonBLead ==true && pTj1>350 && meff >1000 && minDPhiJMET_4>0.4 && DPhiJMET_1>2.8 ) accept("SRB");
  bool SRC_common;
  if (nBaselineLeptons==0 && ETMiss>250 && minDPhiJMET_4>0.4 && nJets>=4 && nbJets>=3) SRC_common =true; 
  else SRC_common= false; 
  //Change the metsig values to those from Vangelis
  if (SRC_common && metsig_HT>22) accept("SRC_incl");
  if (SRC_common && metsig_HT>22 && metsig_HT<24) accept("SRC_22");
  if (SRC_common && metsig_HT>24 && metsig_HT<26) accept("SRC_24");
  if (SRC_common && metsig_HT>26 && metsig_HT<28) accept("SRC_26");
  if (SRC_common && metsig_HT>28) accept("SRC_28");

  //Dump variables into ntuples
  ntupVar("mcChannel",mcChannel);
  ntupVar("genMET",event->getGenMET());
  ntupVar("genHT",event->getGenHT());
  ntupVar("nBaselineLeptons",nBaselineLeptons);
  ntupVar("nJets",nJets);
  ntupVar("nBJets",nbJets);
  ntupVar("pTb1",pTb1);
  ntupVar("pTj1",pTj1);
  ntupVar("ETMiss",ETMiss);
  ntupVar("meff",meff);
  ntupVar("metsig_HT",metsig_HT);
  ntupVar("minDPhiJMET_4",minDPhiJMET_4);
  ntupVar("DPhiJMET_1",DPhiJMET_1);
  ntupVar("maxmin_M",maxmin_M);
  ntupVar("max_avg_M",max_avg_M);
  ntupVar("maxDR",maxDR);
  ntupVar("maxminDR",maxminDR);
  ntupVar("nonBLead",nonBLead);

  return;
}

static bool maxmin_alg(double &maxDR, double &maxminDR, double &max_M, double &maxmin_M, double &max_avg_M, AnalysisObjects bJets){
  
  maxDR=0;
  unsigned int imax = -1;
  unsigned int jmax = -1;
  std::vector<std::vector<double > > DR_matrix;
  for(unsigned int i=0; i<bJets.size(); i++){
    std::vector<double> temp; 
    for(unsigned int j=0; j<=i; j++){
	double DR = bJets[i].DeltaR(bJets[j]);
      if (i !=j){
	if (DR>maxDR) {
	  maxDR = DR;
	  imax = i; 
	  jmax = j;
	}
      }
      temp.push_back(DR);
    }
    DR_matrix.push_back(temp);
  }
  max_M = (bJets[imax]+bJets[jmax]).M();

  maxminDR = 99;
  double maxmaxDR = -1;
  unsigned int imin = -1;
  unsigned int jmin = -1;
  unsigned int imaxmax = -1;
  unsigned int jmaxmax = -1;
  for(unsigned int i=0; i<bJets.size(); i++){
    for(unsigned int j=0; j<=i; j++){
      if (i !=j){
	double DR = DR_matrix[i][j];
	if (i!= imax && j!=jmax && DR>maxmaxDR){
	  imaxmax = i; 
	  jmaxmax = j;
	}
	if (i!= imax && j!=jmax && DR<maxminDR){
	  maxminDR = DR;
	  imin = i;
	  jmin = j;
	}
      }
    }
  }

  double maxmax_M = (bJets[imaxmax]+bJets[jmaxmax]).M();
  max_avg_M = (max_M+maxmax_M)/2;
  maxmin_M = (bJets[imin]+bJets[jmin]).M();
  return true;
}
		       


