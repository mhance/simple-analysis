stages:
  - check
  - build
  - test
  - review
  - deploy

check_md_table:
  image: gitlab-registry.cern.ch/authoring/documentation/s2i-mkdocs-container
  stage: check
  script:
    - grep -q '<!-- DYNAMIC CONTENT HERE -->' docs/src/analyses.md

build_image:
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_SSL_NO_VERIFY: "true"
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - '[[ $CI_COMMIT_REF_SLUG = "master" ]] && DOCKER_TAG="latest" || DOCKER_TAG="$CI_COMMIT_REF_SLUG"'
    - /kaniko/executor --context $CI_PROJECT_DIR
                       --dockerfile $CI_PROJECT_DIR/Dockerfile
                       --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
                       --destination $CI_REGISTRY_IMAGE:$DOCKER_TAG
                       --build-arg CI_COMMIT_SHA=$CI_COMMIT_SHA
                       --build-arg CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
                       --build-arg CI_COMMIT_TAG=$CI_COMMIT_TAG
                       --build-arg CI_JOB_URL=$CI_JOB_URL
                       --build-arg CI_PROJECT_URL=$CI_PROJECT_URL

.base_image:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - source /release_setup.sh

check_cli:
  extends: .base_image
  stage: test
  script:
    - simpleAnalysis -l

# NB: artifacts from all previous stages are passed by default
build_docs:
  stage: build
  image: gitlab-registry.cern.ch/authoring/documentation/s2i-mkdocs-container
  script:
    - ./ci/generate_analysis_table.sh
    - cd docs/
    - pip install -r requirements.txt
    - mkdocs build --clean --strict --site-dir $(mktemp -d)
  artifacts:
    paths:
      - docs/src/analyses.md

.openshift:
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  before_script:
    - oc login "$OPENSHIFT_SERVER" --token="$OPENSHIFT_TOKEN" --insecure-skip-tls-verify
    - oc project $OPENSHIFT_PROJECT_NAME > /dev/null 2>&1

trigger:
  extends: .openshift
  stage: deploy
  script:
    - oc start-build docs-public --from-dir=. --wait --follow
  only:
    refs:
      - master@atlas-sa/simple-analysis

.review:
  extends: .openshift
  stage: review
  script:
    - oc get buildconfig $APP > /dev/null 2>&1 && oc delete all --selector "build=${APP}"
    - oc new-build mkdocs:${MKDOCS_RELEASE:-1.0} --binary --name=$APP
    - oc start-build $APP --from-dir=$BASE_DIR --wait
    - oc get services $APP > /dev/null 2>&1 || oc new-app $APP
    - oc get routes $APP > /dev/null 2>&1 || oc expose service $APP --hostname=$APP_HOST
    - oc annotate route $APP --overwrite router.cern.ch/network-visibility=Internet
  only:
    refs:
      - branches@atlas-sa/simple-analysis
  except:
    refs:
      - master

Deploying review:
  extends: .review
  variables:
    APP: review-public-${CI_COMMIT_REF_SLUG}
    APP_HOST: review-public-${CI_COMMIT_REF_SLUG}-${OPENSHIFT_PROJECT_NAME}.${OPENSHIFT_DOMAIN}
    BASE_DIR: docs/
  environment:
    name: review/public/${CI_COMMIT_REF_SLUG}
    url: http://review-public-${CI_COMMIT_REF_SLUG}-${OPENSHIFT_PROJECT_NAME}.${OPENSHIFT_DOMAIN}
    on_stop: Stopping review
    auto_stop_in: 2 weeks

Stopping review:
  extends: .review
  script:
    - oc delete all --selector "build=${APP}"
    - oc delete all --selector "app=${APP}"
  when: manual
  variables:
    APP: review-public-${CI_COMMIT_REF_SLUG}
    GIT_STRATEGY: none
  environment:
    name: review/public/${CI_COMMIT_REF_SLUG}
    action: stop
