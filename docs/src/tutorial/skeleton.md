---
title: Creating an empty analysis skeleton
---

### Creating an empty skeleton

Let's go ahead and create an analysis from scratch. Touch a new file in `SimpleAnalysis/Root/MyAnalysis.cxx`
```sh
cd $TUTORIAL_DIR/SimpleAnalysis/SimpleAnalysisCodes/src
touch MyAnalysis.cxx
```

### General structure

In general, the structure of an analysis runner is very similar to any other event looping algorithm that you encounter in ATLAS. There is an `AnalysisClass::Init()` and an `AnalysisClass::ProcessEvent(AnalysisEvent *event)` that you need to implement.

The skeleton of an analysis always looks like this:
```cpp
#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(MyAnalysisName)

void MyAnalysisName::Init() {}

void MyAnalysisName::ProcessEvent(AnalysisEvent *event) { return; }
```

The `Init()` method handles initialisation of any regions or histograms that you would like to define in your analysis. The `ProcessEvent()` method is, as the name would suggest, called for each event and is supposed to handle everything from object definition, to overlap removal and event selection.

Using your favorite editor, paste the above analysis skeleton into the newly created `MyAnalysis.cxx` file.

!!! info "Naming convention for analysis implementation files" 
    In a real-world example, the naming convention is for full Run-2 analyses to end in "2018" (i.e. the year of the latest used data). Have a look at the existing analyses if you are not sure about how to name yours.

### Recompilation

After having created and edited the `MyAnalysis.cxx` file, let's go ahead and recompile. Don't forget to tell CMake about the new file we created:
```sh
cd $TUTORIAL_DIR/build # switch to build directory
cmake ../SimpleAnalysis # needed because we added a file
make # recompilation should be quick
```
