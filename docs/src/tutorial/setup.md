---
title: Setting up SimpleAnalysis
---

## Preliminaries

Please make sure you are using CentOS7, since `SimpleAnalysis` is only fully working on that OS. If you are on `lxplus` (e.g. through CERN), then you don't need to worry about having the right architecture and software environment.

## Setup

First, let's create a suitable directory structure for the entire tutorial.

=== "With Docker"

    The docker images available are based on CentOS7. These are pre-built and ready to use so there is no set up needed for you to do. Simply make sure you pull the images

    ```bash
    docker pull gitlab-registry.cern.ch/atlas-sa/simple-analysis:master
    ```

    and you're good to go.


=== "Using lxplus"

    As usual, in ATLAS, we make a `run` and a `build` directory (no `source` directory this time, this will be taken over by the SimpleAnalysis source code that we checkout later).
    ```sh
    export TUTORIAL_DIR=$(pwd) #so we can get around easier and don't get confused with paths
    mkdir build run
    ```
    Now, setup the ATLAS software environment and a more-or-less recent AnalysisBase release
    ```sh
    cd $TUTORIAL_DIR/build
    setupATLAS
    asetup AnalysisBase,21.2.122 # use a recent AnalysisBase release
    ```

    !!! tip "Setup in a virtual environment"
        Alternatively you can setup the AnalysisBase realease using a docker image: Run the analysisbase image, mounting the SimpleAnalysis directory into the image:<code>docker run -v \$TUTORIAL_DIR:/home/atlas/\$TUTORIAL_DIR -it atlas/analysisbase:21.2.122</code>.  Setup the realease and get to the right place in the image:<code>source /release_setup.sh</code>.  cd to the right place: <code> cd /home/atlas/\$TUTORIAL_DIR/build/</code>

    Now setup a recent version of `git` and clone the SimpleAnalysis repository including all the submodules into the base-directory,

    === "authentication"
        ```
        lsetup git
        cd $TUTORIAL_DIR
        git clone --recursive https://:@gitlab.cern.ch:8443/atlas-phys-susy-wg/SimpleAnalysis.git
        ```

    === "ssh keys"
        ```
        lsetup git
        cd $TUTORIAL_DIR
        git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-wg/SimpleAnalysis.git
        ```

    If all goes well, this checks out `SimpleAnalysis` as well as the dependencies `CalcGenericMT2` and `Ext_RestFrames`. If you do an `ls` on the created directory, this is what you should see
    ```sh
    $ ls -l SimpleAnalysis
    -rw-r--r--. 1 eschanet zp  900 Feb  9 08:39 CMakeLists.txt
    -rw-r--r--. 1 eschanet zp 3478 Feb  9 08:39 CODE_OF_CONDUCT.md
    -rw-r--r--. 1 eschanet zp  160 Feb  9 08:39 Dockerfile
    drwxr-xr-x. 4 eschanet zp 2048 Feb  9 08:39 Ext_RestFrames
    -rw-r--r--. 1 eschanet zp 6290 Feb  9 08:39 README.md
    drwxr-xr-x. 4 eschanet zp 2048 Feb  9 08:39 SimpleAnalysisCodes
    drwxr-xr-x. 5 eschanet zp 2048 Feb  9 08:39 SimpleAnalysisFramework
    drwxr-xr-x. 2 eschanet zp 2048 Feb  9 08:39 scripts
    drwxr-xr-x. 2 eschanet zp 2048 Feb  9 08:39 src
    ```

    Next, we go back into the `build` directory and configure CMake
    ```sh
    cd $TUTORIAL_DIR/build
    cmake ../SimpleAnalysis
    ```

    ???+ info "Using *b*-tagging weights"
        You can use `cmake -DDO_TRUTHTAGGING=ON ../SimpleAnalysis` if you need *b*-tagging weights.


    then, we actually compile:
    ```sh
    make
    ```

    ???+ info "Compilation time"
        The very first compilation takes quite some time (typically 15 minutes on lxplus). Subsequent compilations will typically be **a lot** faster. If you don't want to wait for the compilation to finish, feel free to open a new shell and continue with the [inputs](setup.md#inputs) and then [analysis skeleton](skeleton).

    Once compilation has successfully finished, you need to tell your shell where to find the compiled binaries:
    ```sh
    source x86_64-centos7-gcc8-opt/setup.sh
    ```

    ## On every login

    If you open a new shell (i.e. you logout and come back or just open another terminal window), then you will need to setup again the `AnalysisBase` release and tell your shell where to find the compiled binaries.
    ```sh
    cd build # in the build directory
    setupATLAS
    asetup --restore # restores previous release
    source x86_64-centos7-gcc8-opt/setup.sh
    ```

    ## Inputs

    You'll need a couple of input files for this tutorial. The input files are available through EOS in
    ```
    /eos/atlas/atlascerngroupdisk/phys-exotics/cdm/SimpleAna2020_tutorial/inputs/
    ```

    If `/eos/` is mounted on your machine (e.g. because you are on lxplus) you can use standard UNIX commands (see also [this CERN KB article](https://cern.service-now.com/service-portal/article.do?n=KB0004244)) and create a symbolic link (or copy the files over). If you are not on lxplus, and/or `/eos/` is not mounted, you can get the input files through the XRootD protocol (see also [this CERN KB article](https://cern.service-now.com/service-portal/article.do?n=KB0001998)).

    === "UNIX commands"
        ```sh
        # either copy them
        cp -r /eos/atlas/atlascerngroupdisk/phys-exotics/cdm/SimpleAna2020_tutorial/inputs $TUTORIAL_DIR
        # or create symbolic link
        ln -s /eos/atlas/atlascerngroupdisk/phys-exotics/cdm/SimpleAna2020_tutorial/inputs $TUTORIAL_DIR
        ```
    === "XRootD"
        ```sh
        setupATLAS
        lsetup xrootd # setup XRootD protocol
        kinit <username>@CERN.CH # authentication
        xrdcp -r root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-exotics/cdm/SimpleAna2020_tutorial/inputs $TUTORIAL_DIR
        ```
