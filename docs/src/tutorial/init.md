
We will start actually implementing an example analysis now. We will use the [SUSY 1Lbb](https://arxiv.org/abs/1909.09226) analysis as a basis for this example (we could have chosen any other BSM search, this is simply the author's preference...).

This search targets a final state with exactly one lepton, two *b*-tagged jets and high E<sub>T</sub><sup>miss</sup>.

## Regions

Let's start by initialising the necessary regions in the `MyAnalysisName::Init()` method. Telling SimpleAnalysis about regions is as simple as adding
```cpp
addRegions({"region_1", "region_2"});
```
to `MyAnalysisName::Init()`. This method takes an `std::vector` of `std::string`.  SimpleAnalysis will then ensure that a corresponding line in the output file and corresponding `TBranch` in the output ntuple will be created.

The 1Lbb analysis features a number of regions of interest (ROI), a total of 9 exclusion signal regions, 3 discovery signal regions, 6 validation regions and 5 control regions. In this tutorial, we will restrict ourselves to the 9 exclusion signal regions and skip the implementation of the other ROIs (just to keep this example as concise as possible).
```cpp
// Define signal regions
addRegions({"SR_h_Low_bin1", "SR_h_Low_bin2", "SR_h_Low_bin3"});
addRegions({"SR_h_Med_bin1", "SR_h_Med_bin2", "SR_h_Med_bin3"});
addRegions({"SR_h_High_bin1", "SR_h_High_bin2", "SR_h_High_bin3"});
```

!!! warning "Naming convention"
    Use the same names as in the analysis paper (since this code is ideally published with the paper) and avoid `-` (dash) and any non-alphanumeric characters.

!!! info "Adding multiple regions"
    There is not need to organise the ROIs into separate calls of `addRegions()`, it's just a bit more readable if different region types are clearly separated.

Initialising control regions, validation regions or any other region of interest obviously works just the same. Simply initialise it using the `addRegions()` method. If we want to initialise a region called `"preselection"` (which we would use to accept events that pass a certain preselection), we just do
```cpp
// Preselection
addRegion("preselection");
```

Note how we used the singular of `addRegion` here. This method expects a single `std::string` as parameter.

## Histograms

SimpleAnalysis can also output ROOT files with histograms and `TBranches` containing kinematic distributions. SimpleAnalysis allows one-dimensional as well as two-dimensional histograms. Add the following lines to your `MyAnalysisName::Init()` method:
```cpp
// Initialise some exemplary histograms
addHistogram("hist_met",100,0,1000); //1D
addHistogram("hist_mt",100,0,1000); //1D
addHistogram("hist_metvsmt",100,0,1000,100,0,1000); //2D
```
The `addHistogram()` method expects a name and a binning as parameters. SimpleAnalysis will make sure to book these histograms and allow us to fill them in the `processEvent` step later on.

!!! warning "Units"
    Note that in SimpleAnalysis, everything is in GeV and not in MeV (as would be ATLAS standard).

## Summary

That's it already for the initialisation step. Your `MyAnalysisName::Init()` method should now look like this:
```cpp
void MyAnalysisName::Init()
{
  // Define signal regions
  addRegions({"SR_h_Low_bin1", "SR_h_Low_bin2", "SR_h_Low_bin3"});
  addRegions({"SR_h_Med_bin1", "SR_h_Med_bin2", "SR_h_Med_bin3"});
  addRegions({"SR_h_High_bin1", "SR_h_High_bin2", "SR_h_High_bin3"});

  // Preselection
  addRegions({"preselection"});

  // Initialise some exemplary histograms
  addHistogram("hist_met",100,0,1000); //1D
  addHistogram("hist_mt",100,0,1000); //1D
  addHistogram("hist_metvsmt",100,0,1000,100,0,1000); //2D
}
```
