#!/bin/bash
set -e

[ ! -z "$TERM" -a -r /etc/motd ] && cat /etc/motd
source /release_setup.sh
if [ "${1:0:1}" = '-' ]; then
    set -- simpleAnalysis "$@"
fi

exec "$@"
