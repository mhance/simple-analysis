# Simplified ATLAS SUSY analysis framework

Holds a collections of SUSY analyses. These can be run over samples in different
input formats:

 * DAOD_TRUTH      (TRUTH1 and TRUTH3 tested)
 * xAOD            (either truth-level and or reco-level - the latter with some constraints)
 * slimmed ntuples (Reduced ntuples produced from above input)

It provides the analysis acceptance per control and signal region as well as
optionally histograms or ntuples with event level objects.

## Docs

Documentation is found at [https://simpleanalysis.docs.cern.ch](https://simpleanalysis.docs.cern.ch).

## Help

If you need more help as a non-ATLAS user, please contact ATLAS SUSY Working Group Conveners at atlas-phys-susy-conveners (AT) cern.ch.

If you need more help as an ATLAS user, please contact the developers directly at atlas-phys-susy-simpleanalysis-developers (AT) cern.ch.

# Acknowledgements

The ATLAS Collaboration thanks Christopher Rogan for granting explicit permission for [RestFrames](http://restframes.com/) to be used in this code.
